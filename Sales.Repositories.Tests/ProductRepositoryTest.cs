﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Repositories.Repositories.ModelsRepository;
using System.Collections.Generic;
using System.Linq;

namespace Sales.Repositories.Tests
{
    [TestClass]
    public class ProductRepositoryTest
    {

        private readonly ProductRepository _repo;

        public ProductRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<SaleDbContext>().UseInMemoryDatabase(databaseName: "ProductTypeDatabase").Options;

            var dbMock = new SaleDbContext(options);
            dbMock.Product.RemoveRange(dbMock.Product.ToList());
            init(dbMock);
            dbMock.SaveChanges();
            _repo = new ProductRepository(dbMock);
        }

        private void init(SaleDbContext dbMock)
        {
            var productGenders = GenerateProductGender();
            var productTypes = GenerateProductTypes(productGenders);
            dbMock.Product.Add(new Product() { });
        }

        private List<ProductGender> GenerateProductGender()
        {
            var result = new List<ProductGender>();
            for (int i = 0; i <= 10; i++)
            {
                ProductGender p = new ProductGender() { Id = i, Type = $"{i}" };
                result.Add(p);
            }
            return result;
        }

        private List<ProductType> GenerateProductTypes(List<ProductGender> productGenders)
        {
            var result = new List<ProductType>();
            for (int i = 0; i < 10; i++)
            {
                ProductType p = new ProductType() { Genders = { productGenders[i], productGenders[i + 1] }, Id = i, Rating = i * 2, Title = $"{i}" };
                result.Add(p);
            }
            return result;
        }

    }
}
