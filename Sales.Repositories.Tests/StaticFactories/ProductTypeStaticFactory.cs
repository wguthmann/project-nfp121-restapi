﻿using Sales.Model.Entities;

namespace Sales.Repositories.Tests.StaticFactories
{
    public class ProductTypeStaticFactory
    {
        public static ProductType CreateLeagueOfClan()
        {
            return new ProductType() { Id = 1, Rating = 13, Title = "League Of Clan" };
        }
        public static ProductType CreateSuperYoshi46()
        {
            return new ProductType() { Id = 4, Rating = 6, Title = "Super Yoshi 46" };
        }
        public static ProductType CreateGodOfWarcraft()
        {
            return new ProductType()
            { Id = 3, Rating = 18, Title = "God of Warcraft" };
        }
        public static ProductType CreateRaidSmiteLegend()
        {
            return new ProductType() { Id = 2, Rating = 6, Title = "Raid Smite Legend" };
        }

    }
}
