﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Repositories.Repositories.ModelsRepository;
using System.Linq;


namespace Sales.Repositories.Tests
{
    [TestClass]
    public class ProductTypeRepositoryTest
    {
        private readonly ProductTypeRepository _repo;
        public ProductTypeRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<SaleDbContext>().UseInMemoryDatabase(databaseName: "ProductTypeDatabase").Options;

            var dbMock = new SaleDbContext(options);
            dbMock.ProductType.RemoveRange(dbMock.ProductType.ToList());
            init(dbMock);
            dbMock.SaveChanges();
            _repo = new ProductTypeRepository(dbMock);
        }

        private void init(SaleDbContext dbMock)
        {
            dbMock.ProductType.Add(new ProductType() { Id = 1, Rating = 13, Title = "League Of Clan" });
            dbMock.ProductType.Add(new ProductType() { Id = 2, Rating = 6, Title = "Raid Smite Legend" });
            dbMock.ProductType.Add(new ProductType() { Id = 3, Rating = 18, Title = "God of Warcraft" });
            dbMock.ProductType.Add(new ProductType() { Id = 4, Rating = 6, Title = "Super Yoshi 46" });
        }


        [TestMethod]
        public void Get_With3_ThenTitleIs_GodOfWarcraft()
        {
            var res = _repo.Get(3);
            Assert.AreEqual("God of Warcraft", res.Title);
        }

        [TestMethod]
        public void GetAll_Then4Result()
        {
            var res = _repo.All();
            Assert.AreEqual(4, res.Count);
        }

        [TestMethod]
        public void Delete_WhithExistingObject_ThenTrue()
        {
            var res = _repo.All().First();
            Assert.IsTrue(_repo.Remove(res.Id));
        }

        [TestMethod]
        public void Delete_WhithNotExistingObject_ThenFalse()
        {
            var res = new ProductType();
            Assert.IsFalse(_repo.Remove(res.Id));
        }

        [TestMethod]
        public void Update3_WithTitleGodOfWarcraft_ThenTitleIsFortCraft()
        {
            var res = _repo.Get(3);
            res.Title = "FortCraft";
            Assert.AreEqual(_repo.Update(res).Title, "FortCraft");
        }

        [TestMethod]
        public void AddProductType_ThenProductType5TitleEqualFortCraft()
        {
            ProductType p = new ProductType() { Rating = 16, Title = "FortCraft" };
            var res = _repo.Add(p);
            Assert.AreEqual(p.Title, _repo.Get(5).Title);
        }


        // TODO: Supprimer ?
        //[TestMethod]
        //public void GetBy_WithRating6_Then2Result()
        //{
        //    ProductType p = new ProductType() { Rating = 6 };
        //    var res = _repo.GetBy(p);
        //    Assert.AreEqual(2, res.Count);
        //}

        //[TestMethod]
        //public void GetBy_WithRating6AndTitleSuperYoshi46_Then1Result()
        //{
        //    ProductType p = new ProductType() { Rating = 6, Title = "Super Yoshi 46" };
        //    var res = _repo.GetBy(p);
        //    Assert.AreEqual(1, res.Count);
        //}

        // [TestMethod]
        // public void GetByGender_With_Then()
        //{
        //    ProductGender p1 = new ProductGender() { Id = 1, Type = "Action" };
        //    ProductType p = new ProductType() { Genders = { p1 } };
        //    var res = _repo.GetByGender(p);
        //    Assert.AreEqual(2, res.Count);
        //}
    }
}
