﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Repositories.Repositories.ModelsRepository;
using System.Linq;

namespace Sales.Repositories.Tests
{
    [TestClass]
    public class UserRepositoryTester
    {

        private readonly UserRepository _repo;

        public UserRepositoryTester()
        {
            var options = new DbContextOptionsBuilder<SaleDbContext>().UseInMemoryDatabase(databaseName: "ProductTypeDatabase").Options;

            var dbMock = new SaleDbContext(options);
            dbMock.User.RemoveRange(dbMock.User.ToList());
            init(dbMock);
            dbMock.SaveChanges();
            _repo = new UserRepository(dbMock);
        }

        private void init(SaleDbContext dbMock)
        {
            dbMock.User.Add(new User() { Id = 1, Username = "Jean-Paul", EmailAddress = "jpp@gmail.com", Password = "password", City = new City() });
            dbMock.User.Add(new User() { Id = 2, Username = "MarieLaPolyglotteENerve", EmailAddress = "Mlepen@gmail.com", Password = "password", City = new City() });
            dbMock.User.Add(new User() { Id = 3, Username = "Melanchon", EmailAddress = "Melanchnous@gmail.com", Password = "password", City = new City() });
        }

        [TestMethod]
        public void Get_With2_ThenUsernameIs_MarieLaPolyglotteENerve()
        {
            var res = _repo.Get(2);
            Assert.AreEqual("MarieLaPolyglotteENerve", res.Username);
        }



        [TestMethod]
        public void GetAll_Then3Result()
        {
            var res = _repo.All();
            Assert.AreEqual(3, res.Count);
        }
    }
}
