﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Repositories.Repositories.ModelsRepository;
using System.Linq;

namespace Sales.Repositories.Tests
{
    [TestClass]
    public class ProductGenderRepositoryTest
    {
        private readonly ProductGenderRepository _repo;
        public ProductGenderRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<SaleDbContext>().UseInMemoryDatabase(databaseName: "ProductGenderDatabase").Options;

            var dbMock = new SaleDbContext(options);
            dbMock.ProductGender.RemoveRange(dbMock.ProductGender.ToList());
            init(dbMock);
            dbMock.SaveChanges();
            _repo = new ProductGenderRepository(dbMock);
        }

        private void init(SaleDbContext dbMock)
        {
            dbMock.ProductGender.Add(new ProductGender() { Id = 1, Type = "Action" });
            dbMock.ProductGender.Add(new ProductGender() { Id = 2, Type = "Adventure" });
            dbMock.ProductGender.Add(new ProductGender() { Id = 3, Type = "Platformer" });
        }

        [TestMethod]
        public void GetByType_WithAction_ThenResultTypeAction()
        {
            var res = _repo.GetByType("Action");
            Assert.AreEqual("Action", res.Type);
        }
    }
}
