﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Repositories.Repositories.ModelsRepository;
using Sales.Services.Validators;
using System.Linq;

namespace Sales.Repositories.Tests.ValidatorTest
{
    [TestClass]
    public class ProductTypeValidatorTest
    {
        private readonly ProductTypeRepository _repo;
        private ProductTypeValidator validator;
        public ProductTypeValidatorTest()
        {
            var options = new DbContextOptionsBuilder<SaleDbContext>().UseInMemoryDatabase(databaseName: "ProductTypeDatabase").Options;

            var dbMock = new SaleDbContext(options);
            dbMock.ProductType.RemoveRange(dbMock.ProductType.ToList());
            _repo = new ProductTypeRepository(dbMock);
            validator = new ProductTypeValidator(_repo);
        }

        [TestMethod]
        public void IsValid_WithValid_ThenValid()
        {
            ProductType product = new ProductType() { Title = "Valid", Rating = 5, Id = 1 };
            var res = validator.IsValid(product);
            Assert.IsTrue(res);
        }

        [TestMethod]
        public void IsValid_WithInValid_ThenInValid()
        {
            ProductType product = new ProductType() { Title = "Valid", Rating = -1, Id = -1 };
            var res = validator.IsValid(product);
            Assert.IsTrue(res);
        }
    }
}
