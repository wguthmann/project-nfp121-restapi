-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.5.9-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Listage des données de la table db_sales.city : ~12 rows (environ)
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` (`Id`, `CityName`, `Country`) VALUES
	(1, 'Strasbourg', 'France'),
	(2, 'Mulhouse', 'France'),
	(3, 'Colmar', 'France'),
	(4, 'Haguenau', 'France'),
	(5, 'Saverne', 'France'),
	(6, 'Molsheim', 'France'),
	(7, 'Strasbourg', 'France'),
	(8, 'Mulhouse', 'France'),
	(9, 'Colmar', 'France'),
	(10, 'Haguenau', 'France'),
	(11, 'Saverne', 'France'),
	(12, 'Molsheim', 'France');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;

-- Listage des données de la table db_sales.conversation : ~8 rows (environ)
/*!40000 ALTER TABLE `conversation` DISABLE KEYS */;
INSERT INTO `conversation` (`Id`, `SendingUserId`, `ReceiverUserId`) VALUES
	(1, 1, 2),
	(2, 2, 3),
	(3, 3, 4),
	(4, 4, 1),
	(5, 1, 2),
	(6, 2, 3),
	(7, 3, 4),
	(8, 4, 1);
/*!40000 ALTER TABLE `conversation` ENABLE KEYS */;

-- Listage des données de la table db_sales.feedback : ~0 rows (environ)
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;

-- Listage des données de la table db_sales.message : ~8 rows (environ)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` (`Id`, `Text`, `LinkPicture`, `SendByUserId`, `ConversationId`) VALUES
	(1, 'Puis-je savoir pourquoi vous vendez ce jeu ?', NULL, 1, 1),
	(2, 'Bonjour est-ce que l\'article que vous vendez est toujours disponible ?', NULL, 3, 2),
	(3, 'Bonjour, effectivement l\'article est toujours en vente.', NULL, 2, 2),
	(4, 'Bonjour, l\'état du jeu est-il bon ?', NULL, 4, 3),
	(5, 'Puis-je savoir pourquoi vous vendez ce jeu ?', NULL, 1, 1),
	(6, 'Bonjour est-ce que l\'article que vous vendez est toujours disponible ?', NULL, 3, 2),
	(7, 'Bonjour, effectivement l\'article est toujours en vente.', NULL, 2, 2),
	(8, 'Bonjour, l\'état du jeu est-il bon ?', NULL, 4, 3);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;

-- Listage des données de la table db_sales.product : ~14 rows (environ)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`Id`, `Title`, `Description`, `ProductPrice`, `CityId`, `OwnerUserId`, `ProductTypeId`, `PhysicalProduct`) VALUES
	(1, 'GTAIV', 'Vends jeu GTAIV en très bon état avec sa pochette d\'origine', 5.9, 2, 1, 1, b'1'),
	(2, 'Halo4', 'Code Xbox live pour Halo4', 10, 5, 2, 4, b'0'),
	(3, 'Farming Simulator', 'Jeu PC Farming Simulator avec tous les éléments d\'origine', 15, 3, 4, 5, b'1'),
	(4, 'Cyberpunk 2077', 'Code de jeu PC pour Cyberpunk 2077 plateforme GOG', 57, 4, 3, 6, b'0'),
	(5, 'Assassin\'s creed Odyssey', 'Vends jeu Assassin\'s Creeed Odyssey en très bon état avec sa pochette d\'origine', 23, 2, 1, 1, b'1'),
	(6, 'NeedForSpeed Payout', 'NeedForSpeed Payout boîte un peu cassé mais CD niquel', 9, 3, 4, 5, b'1'),
	(7, 'SuperMarioBros', 'Jeu super et convivial pour des parties entre amis', 12, 5, 2, 7, b'1'),
	(8, 'GTAIV', 'Vends jeu GTAIV en très bon état avec sa pochette d\'origine', 5.9, 2, 1, 1, b'1'),
	(9, 'Halo4', 'Code Xbox live pour Halo4', 10, 5, 2, 4, b'0'),
	(10, 'Farming Simulator', 'Jeu PC Farming Simulator avec tous les éléments d\'origine', 15, 3, 4, 5, b'1'),
	(11, 'Cyberpunk 2077', 'Code de jeu PC pour Cyberpunk 2077 plateforme GOG', 57, 4, 3, 6, b'0'),
	(12, 'Assassin\'s creed Odyssey', 'Vends jeu Assassin\'s Creeed Odyssey en très bon état avec sa pochette d\'origine', 23, 2, 1, 1, b'1'),
	(13, 'NeedForSpeed Payout', 'NeedForSpeed Payout boîte un peu cassé mais CD niquel', 9, 3, 4, 5, b'1'),
	(14, 'SuperMarioBros', 'Jeu super et convivial pour des parties entre amis', 12, 5, 2, 7, b'1');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Listage des données de la table db_sales.productgender : ~0 rows (environ)
/*!40000 ALTER TABLE `productgender` DISABLE KEYS */;
/*!40000 ALTER TABLE `productgender` ENABLE KEYS */;

-- Listage des données de la table db_sales.producttype : ~14 rows (environ)
/*!40000 ALTER TABLE `producttype` DISABLE KEYS */;
INSERT INTO `producttype` (`Id`, `Title`, `Rating`) VALUES
	(1, 'Jeu PS3', 1),
	(2, 'Code jeu PS3', 2),
	(3, 'Jeu Xbox', 3),
	(4, 'Code jeu Xbox', 4),
	(5, 'Jeu PC', 5),
	(6, 'Code jeu PC', 6),
	(7, 'Autre', 7),
	(8, 'Jeu PS3', 1),
	(9, 'Code jeu PS3', 2),
	(10, 'Jeu Xbox', 3),
	(11, 'Code jeu Xbox', 4),
	(12, 'Jeu PC', 5),
	(13, 'Code jeu PC', 6),
	(14, 'Autre', 7);
/*!40000 ALTER TABLE `producttype` ENABLE KEYS */;

-- Listage des données de la table db_sales.proposition : ~8 rows (environ)
/*!40000 ALTER TABLE `proposition` DISABLE KEYS */;
INSERT INTO `proposition` (`Id`, `ProposalPrice`, `ExchangeProductId`, `Accepted`, `ProposerUserId`, `SaleId`) VALUES
	(1, 15, 1, b'0', 1, 1),
	(2, 22, 2, NULL, 2, 2),
	(3, 36, 3, b'1', 4, 3),
	(4, 7, 4, NULL, 3, 4),
	(5, 15, 1, b'0', 1, 1),
	(6, 22, 2, NULL, 2, 2),
	(7, 36, 3, b'1', 4, 3),
	(8, 7, 4, NULL, 3, 4);
/*!40000 ALTER TABLE `proposition` ENABLE KEYS */;

-- Listage des données de la table db_sales.sale : ~8 rows (environ)
/*!40000 ALTER TABLE `sale` DISABLE KEYS */;
INSERT INTO `sale` (`Id`, `Price`, `BuyerId`, `SellerId`, `SaleProductId`) VALUES
	(1, 5, 1, 2, 1),
	(2, 8, 1, 4, 2),
	(3, 15, 2, 3, 3),
	(4, 57, 3, 1, 4),
	(5, 5, 1, 2, 1),
	(6, 8, 1, 4, 2),
	(7, 15, 2, 3, 3),
	(8, 57, 3, 1, 4);
/*!40000 ALTER TABLE `sale` ENABLE KEYS */;

-- Listage des données de la table db_sales.user : ~8 rows (environ)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`Id`, `Username`, `EmailAddress`, `Password`, `CityId`) VALUES
	(1, 'kk', 'kknittel@gmail.com', 'moulaga', 1),
	(2, 'yh', 'yhirschmann@gmail.com', 'goulaga', 2),
	(3, 'ld', 'ldeffinis@gmail.com', 'poulaga', 3),
	(4, 'wg', 'wguthmann@gmail.com', 'roulaga', 4),
	(5, 'kk', 'kknittel@gmail.com', 'moulaga', 1),
	(6, 'yh', 'yhirschmann@gmail.com', 'goulaga', 2),
	(7, 'ld', 'ldeffinis@gmail.com', 'poulaga', 3),
	(8, 'wg', 'wguthmann@gmail.com', 'roulaga', 4);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Listage des données de la table db_sales.userrating : ~0 rows (environ)
/*!40000 ALTER TABLE `userrating` DISABLE KEYS */;
/*!40000 ALTER TABLE `userrating` ENABLE KEYS */;

-- Listage des données de la table db_sales.__efmigrationshistory : ~1 rows (environ)
/*!40000 ALTER TABLE `__efmigrationshistory` DISABLE KEYS */;
INSERT INTO `__efmigrationshistory` (`MigrationId`, `ProductVersion`) VALUES
	('20210525112551_william-migration', '3.1.1');
/*!40000 ALTER TABLE `__efmigrationshistory` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
