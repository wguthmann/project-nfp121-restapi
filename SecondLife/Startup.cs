using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Repositories.Repositories.ModelsRepository;
using Sales.Services.Services;
using Sales.Services.Services.JwtManager;
using Sales.Services.Services.ModelsService;
using Sales.Services.Services.Seeders;
using Sales.Services.Validators;
using Sales.Services.Validators.Generics;
using SecondLife.Extensions;
using System.Text;

namespace SecondLife
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddControllersWithViews().AddNewtonsoftJson();

            InjectServices(services);
            InjectRepositories(services);
            InjectSeeder(services);

            // JWT
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtAuthenticationManager.Key)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddScoped<IJwtAuthenticationManager, JwtAuthenticationManager>();

            var cs = Configuration.GetConnectionString("Sale");
            services.AddDbContextPool<SaleDbContext>(x =>
            x.UseMySql(cs));
        }

        private static void InjectServices(IServiceCollection services)
        {
            services.AddScoped(typeof(IService<>), typeof(GenericService<>));
            services.AddScoped(typeof(IValidator<>), typeof(Validator<>));
            services.AddScoped<IService<User>, UserService>();
            services.AddScoped<IService<Conversation>, ConversationService>();
            services.AddScoped<IService<Message>, MessageService>();
            services.AddScoped<IValidator<User>, UserValidator>();
            services.AddScoped<IValidator<City>, CityValidator>();
            services.AddScoped<IValidator<Message>, MessageValidator>();
            services.AddScoped<IValidator<Proposition>, PropositionValidator>();
        }

        private static void InjectSeeder(IServiceCollection services)
        {
            services.AddScoped<ISeeder<City>, SeederCities>();
            services.AddScoped<ISeeder<User>, SeederUsers>();
            services.AddScoped<ISeeder<ProductGender>, SeederProductGenders>();
            services.AddScoped<ISeeder<Conversation>, SeederConversations>();
            services.AddScoped<ISeeder<Message>, SeederMessages>();
            services.AddScoped<ISeeder<ProductType>, SeederProductTypes>();
            services.AddScoped<ISeeder<Product>, SeederProducts>();
            services.AddScoped<ISeeder<Conversation>, SeederConversations>();
            services.AddScoped<ISeeder<Proposition>, SeederPropositions>();
            services.AddScoped<ISeeder<Sale>, SeederSales>();
            services.AddScoped<GlobalSeeder>();
        }

        private static void InjectRepositories(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));
            services.AddScoped<IRepository<User>, UserRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IRepository<Conversation>, ConversationRepository>();
            services.AddScoped<IRepository<Message>, MessageRepository>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.EnsureMigrationOfContext<SaleDbContext>();
        }
    }
}
