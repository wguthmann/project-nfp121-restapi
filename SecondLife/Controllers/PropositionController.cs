﻿using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class PropositionController : GenericController<Proposition>
    {
        public PropositionController(IService<Proposition> service, IValidator<Proposition> validator) : base(service, validator)
        {
        }
    }
}
