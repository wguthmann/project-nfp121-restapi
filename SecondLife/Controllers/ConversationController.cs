﻿using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Services.ModelsService;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;
using System.Collections.Generic;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class ConversationController : GenericController<Conversation>
    {
        private new ConversationService _service;

        public ConversationController(IService<Conversation> service, IValidator<Conversation> validator) : base(service, validator)
        {
            _service = service as ConversationService;
        }


        [HttpGet("users/")]
        public ActionResult<List<Conversation>> GetByUsers(int SendingUserId, int ReceiverUserId)
        {
            List<Conversation> res = _service.GetByUsers(SendingUserId, ReceiverUserId);
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

        [HttpGet("implied/{UserId}")]
        public ActionResult<List<Conversation>> GetUserImplied(int UserId)
        {
            List<Conversation> res = _service.GetUserImplied(UserId);
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

        [HttpGet("messages/{ConversationId}")]
        public ActionResult<List<Message>> GetMessagesByConversation(int ConversationId)
        {
            List<Message> res = _service.GetMessagesByConversation(ConversationId);
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

    }
}
