﻿using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class FeedbackController : GenericController<Feedback>
    {
        public FeedbackController(IService<Feedback> service, IValidator<Feedback> validator) : base(service, validator)
        {
        }


    }
}
