﻿using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services.ModelsService;
using SecondLife.Controllers.Generics;
using System.Collections.Generic;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class PreferencesController : GenericController<Preferences>
    {
        private PreferencesService _service;

        public PreferencesController(Sales.Services.Services.IService<Preferences> service, Sales.Services.Validators.Generics.IValidator<Preferences> validator) : base(service, validator)
        {
            _service = service as PreferencesService;
        }

        [HttpPost]
        public ActionResult<List<Product>> GetPreferences(int UserId)
        {
            var result = _service.GetPreferences(UserId);
            if (result == null)
            {
                return NoContent();
            }
            return Ok(result);
        }

    }
}
