﻿using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class CityController : GenericController<City>
    {

        public CityController(IService<City> service, IValidator<City> validator) : base(service, validator)
        {
        }
    }
}