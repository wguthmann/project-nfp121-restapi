﻿using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Services.ModelsService;
using Sales.Services.Validators;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class MessageController : GenericController<Message>
    {
        private new readonly MessageService _service;
        private new readonly MessageValidator _validator;

        public MessageController(IService<Message> service, IValidator<Message> validator) : base(service, validator)
        {
            _service = service as MessageService;
            _validator = validator as MessageValidator;
        }
    }
}
