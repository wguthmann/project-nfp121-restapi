﻿using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Services.ModelsService;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;
using System;
using System.Collections.Generic;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class ProductTypeController : GenericController<ProductType>
    {
        private new readonly ProductTypeService _service;

        public ProductTypeController(IService<ProductType> service, IValidator<ProductType> validator) : base(service, validator)
        {
            _service = service as ProductTypeService;
        }

        [HttpGet]
        public ActionResult<List<ProductType>> GetBy(int rating = default, DateTime date = default, string title = "")
        {
            ProductType productType = new ProductType() { Rating = rating, Title = title };
            return _service.GetBy(productType);
        }

    }
}
