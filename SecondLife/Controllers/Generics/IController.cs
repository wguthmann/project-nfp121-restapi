﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace SecondLife.Controllers.Generics
{
    public interface IController<T> where T : class
    {
        public ActionResult<T> Create(T obj);

        public ActionResult<List<T>> List();

        public ActionResult<T> Get(int id);

        public ActionResult<T> JsonPatch(int id,
           [FromBody] JsonPatchDocument<T> obj);

        public ActionResult<T> Delete(int id);
    }
}