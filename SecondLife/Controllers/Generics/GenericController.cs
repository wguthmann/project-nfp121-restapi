﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Sales.Services.Services;
using Sales.Services.Validators.Generics;
using System.Collections.Generic;

namespace SecondLife.Controllers.Generics
{
    public class GenericController<T> : ControllerBase, IController<T> where T : class
    {
        protected IService<T> _service;
        protected IValidator<T> _validator;

        public GenericController(IService<T> service, IValidator<T> validator)
        {
            _service = service;
            _validator = validator;
        }

        [HttpPost]
        public ActionResult<T> Create([FromBody] T obj)
        {
            if (_validator.CanAdd(obj))
            {
                T res = _service.Add(obj);
                if (res == null)
                {
                    return BadRequest("invalid request");
                }
                return Ok(res);
            }

            return BadRequest("Cannot add existing " + obj);
        }

        [HttpGet]
        public ActionResult<List<T>> List()
        {

            List<T> res = _service.List();
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;

        }

        [HttpDelete("{id}")]
        public ActionResult<T> Delete(int id)
        {
            var boolRes = _service.Remove(id);
            if (boolRes)
                return Ok();
            return BadRequest("Error removing object");

        }

        [HttpGet("{id}")]
        public ActionResult<T> Get(int id)
        {
            return _service.Get(id);
        }

        [HttpPatch("{id}")]
        public ActionResult<T> JsonPatch(int id, [FromBody] JsonPatchDocument<T> obj)
        {
            T res = _service.Patch(id, obj);
            if (res == null)
                return BadRequest();
            return Ok(res);

        }
    }
}
