﻿using Microsoft.AspNetCore.Mvc;

namespace SecondLife.Controllers
{
    [ApiController, Route("/")]
    public class HealthCheckController : ControllerBase
    {
        [HttpGet("")]
        public ActionResult healthcheck()
        {
            return Ok();
        }
    }
}
