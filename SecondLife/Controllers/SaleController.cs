﻿using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class SaleController : GenericController<Sale>
    {
        public SaleController(IService<Sale> service, IValidator<Sale> validator) : base(service, validator)
        {
        }
    }

}
