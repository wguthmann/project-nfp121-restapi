﻿using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;

namespace SecondLife.Controllers
{

    [ApiController, Route("api/[controller]")]
    public class ProductController : GenericController<Product>
    {
        public ProductController(IService<Product> service, IValidator<Product> validator) : base(service, validator)
        {
        }
    }

}
