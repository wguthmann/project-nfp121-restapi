using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;

namespace SecondLife.Controllers
{

    [ApiController, Route("api/[controller]")]
    public class ProductGenderController : GenericController<ProductGender>
    {
        public ProductGenderController(IService<ProductGender> service, IValidator<ProductGender> validator) : base(service, validator)
        {
        }
    }
}
