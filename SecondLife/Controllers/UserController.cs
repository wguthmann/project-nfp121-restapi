﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using Sales.Services.Services.JwtManager;
using Sales.Services.Services.ModelsService;
using Sales.Services.Validators;
using Sales.Services.Validators.Generics;
using SecondLife.Controllers.Generics;
using System.Collections.Generic;

namespace SecondLife.Controllers
{
    //[Authorize]
    [ApiController, Route("api/[controller]")]
    public class UserController : GenericController<User>
    {
        private new readonly UserService _service;
        private new readonly UserValidator _validator;
        private readonly IJwtAuthenticationManager _jwtAuthenticationManager;

        public UserController(IService<User> service, IValidator<User> validator, IJwtAuthenticationManager jwtAuthenticationManager) : base(service, validator)
        {
            _service = service as UserService;
            _validator = validator as UserValidator;
            _jwtAuthenticationManager = jwtAuthenticationManager;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] UserCred userCred)
        {
            var token = _jwtAuthenticationManager.Authenticate(userCred.Username, userCred.Password);

            if (token == null)
                return Unauthorized();

            return Ok(token);
        }


        [HttpPost("validatecredentials")]
        public ActionResult<User> ValidateCredentials([FromBody] User user)
        {
            return _service.ValidateCredentials(user.Password, user.Username);
        }


        [HttpGet("feedback/{UserId}")]
        public ActionResult<List<Feedback>> GetFeedback(int UserId)
        {
            User user = _service.Get(UserId);
            List<Feedback> res = _service.GetFeedback(user);
            if (res.Count == 0)
            {
                return NoContent();
            }

            return res;
        }

        [HttpGet("avgrate/{UserId}")]
        public ActionResult<float> GetAverageRate(int UserId)
        {
            User user = _service.Get(UserId);
            float res = _service.GetAverageRate(user);
            return res;
        }


        // TODO: Supprimer ?
        //[HttpGet("{id}/feedback")]
        //public ActionResult<List<Feedback>> GetFeedback(int id)
        //{
        //    User user = _service.Get(id);
        //    List<Feedback> res = _service.GetFeedback(user);
        //    if (res.Count == 0)
        //    {
        //        return NoContent();
        //    }
        //    return res;
        //}

        [HttpGet("rate/")]
        public ActionResult<bool> AddUserRating(int UserId, int Rating)
        {
            if (UserId > 0)
            {
                var res = _service.AddUserRating(UserId, Rating);
                if (res)
                    return Accepted();
            }

            return BadRequest("Error while trying to rate User");
        }
    }
}
