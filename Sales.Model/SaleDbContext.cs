﻿using Microsoft.EntityFrameworkCore;
using Sales.Model.Entities;

namespace Sales.Model
{
    public class SaleDbContext : DbContext
    {
        public DbSet<City> City { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Feedback> Feedback { get; set; }
        public DbSet<ProductType> ProductType { get; set; }
        public DbSet<Proposition> Proposition { get; set; }
        public DbSet<ProductGender> ProductGender { get; set; }
        public DbSet<Conversation> Conversation { get; set; }
        public DbSet<Message> Message { get; set; }
        public DbSet<Sale> Sale { get; set; }
        public DbSet<User> User { get; set; }

        public SaleDbContext(DbContextOptions<SaleDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(
                    "server=localhost;port=3306;database=db_sales;uid=wguthmann;password=password;TreatTinyAsBoolean=false");
            }
            base.OnConfiguring(optionsBuilder);
        }
    }
}
