﻿namespace Sales.Model.Interface
{
    public interface IIdentityObject
    {
        public int Id { get; set; }
    }
}
