﻿using Sales.Model.Interface;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Sales.Model.Entities
{
    public class Proposition : IIdentityObject

    {
        [Key]
        public int Id { get; set; }

        [Required]
        public float ProposalPrice { get; set; }

        [AllowNull]
        public Product ExchangeProduct { get; set; }

        [AllowNull]
        public bool? Accepted { get; set; }

        [Required]
        public User ProposerUser { get; set; }

        [Required]
        public Sale Sale { get; set; }


        public Proposition()
        {
        }


        public Proposition(float proposalPrice, Product exchangeProduct, bool? accepted, User proposerUser, Sale sale)
        {
            ProposalPrice = proposalPrice;
            ExchangeProduct = exchangeProduct;
            Accepted = accepted;
            ProposerUser = proposerUser;
            Sale = sale;
        }
    }
}
