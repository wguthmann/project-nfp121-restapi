﻿using Sales.Model.Interface;
using System.ComponentModel.DataAnnotations;

namespace Sales.Model.Entities
{
    public class City : IIdentityObject
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string CityName { get; set; }

        [Required]
        public string Country { get; set; }


        public City(string cityName, string country)
        {
            CityName = cityName;
            Country = country;
        }


        public City()
        {
        }
    }
}
