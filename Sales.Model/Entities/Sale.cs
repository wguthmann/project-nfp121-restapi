﻿using Sales.Model.Interface;
using System.ComponentModel.DataAnnotations;

namespace Sales.Model.Entities
{
    public class Sale : IIdentityObject
    {
        [Key]
        public int Id { get; set; }

        public float Price { get; set; }

        public User Buyer { get; set; }

        [Required]
        public User Seller { get; set; }

        [Required]
        public Product SaleProduct { get; set; }


        public Sale()
        {
        }


        public Sale(float price, User buyer, User seller, Product saleProduct)
        {
            Price = price;
            Buyer = buyer;
            Seller = seller;
            SaleProduct = saleProduct;
        }
    }
}
