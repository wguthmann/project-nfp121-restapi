﻿using Newtonsoft.Json;
using Sales.Model.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Sales.Model.Entities
{
    public class User : IIdentityObject
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Username { get; set; }

        [NotNull] // Pas Required sinon ValidateCredentials ne fonctionne pas car le constructeur nécessiterait ce champ
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Required]
        //[JsonIgnore]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        
        public City City { get; set; }

        [AllowNull]
        public List<ProductGender> Preferences { get; set; }

        [AllowNull]
        public List<UserRating> UserRatings { get; set; }


        public User(string username, string emailAddress, string password, City city)
        {
            Username = username;
            EmailAddress = emailAddress;
            Password = password;
            City = city;
        }


        // Used to Login (ValidateCredentials), not to write in db
        public User(string username, string password)
        {
            Username = username;
            Password = password;
        }


        public User()
        {

        }

        public List<ProductGender> addPreference(ProductGender p)
        {
            Preferences.Add(p);
            return Preferences;
        }


        public List<ProductGender> removePreference(ProductGender p)
        {
            Preferences.Remove(p);
            return Preferences;
        }

        public static explicit operator User((string, string, string, City) v)
        {
            throw new NotImplementedException();
        }
    }
}
