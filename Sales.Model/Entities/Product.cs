﻿using Sales.Model.Interface;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Sales.Model.Entities
{
    public class Product : IIdentityObject
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [AllowNull]
        public string Description { get; set; }

        //[Required]
        public float ProductPrice { get; set; }

        [AllowNull]
        public City City { get; set; }

        //[Required]
        public User OwnerUser { get; set; }

        //[Required]
        public virtual ProductType ProductType { get; set; }

        [AllowNull]
        public bool PhysicalProduct { get; set; }


        public Product()
        {
        }


        public Product(string title, string description, float productPrice, City city, User ownerUser, ProductType productType, bool physicalProduct)
        {
            Title = title;
            Description = description;
            ProductPrice = productPrice;
            City = city;
            OwnerUser = ownerUser;
            ProductType = productType;
            PhysicalProduct = physicalProduct;
        }
    }
}
