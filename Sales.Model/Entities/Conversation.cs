﻿using Sales.Model.Interface;
using System.ComponentModel.DataAnnotations;

namespace Sales.Model.Entities
{
    public class Conversation : IIdentityObject
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public User SendingUser { get; set; }

        [Required]
        public User ReceiverUser { get; set; }

        public Conversation()
        {
        }

        public Conversation(User sendingUser, User receiverUser)
        {
            SendingUser = sendingUser;
            ReceiverUser = receiverUser;
        }
    }
}
