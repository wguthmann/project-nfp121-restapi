﻿using Sales.Model.Interface;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Sales.Model.Entities
{
    /**
     * Représente le support de jeu : Code d'activation, jeu dématérialisé, jeu physique... 
     */
    public class ProductType : IIdentityObject
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [AllowNull]
        public int Rating { get; set; }

        [AllowNull]
        public List<ProductGender> Genders { get; set; }


        public ProductType()
        {
        }


        public ProductType(string Title, int Rating, List<ProductGender> Genders)
        {
            this.Title = Title;
            this.Rating = Rating;
            this.Genders = Genders;
        }


        public List<ProductGender> addGender(ProductGender p)
        {
            Genders.Add(p);
            return Genders;
        }


        public List<ProductGender> RemoveGenders(ProductGender p)
        {
            Genders.Remove(p);
            return Genders;
        }
    }
}
