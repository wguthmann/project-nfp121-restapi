﻿using Sales.Model.Interface;
using System.ComponentModel.DataAnnotations;

namespace Sales.Model.Entities
{
    /**
     * Représente le style de jeu : FPS, TPS, MMO RPG, Free to play...
     */
    public class ProductGender : IIdentityObject
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Type { get; set; }

        public ProductGender()
        {
        }

        public ProductGender(string type)
        {
            Type = type;
        }
    }
}