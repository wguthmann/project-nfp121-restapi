﻿using Sales.Model.Interface;
using System.ComponentModel.DataAnnotations;

namespace Sales.Model.Entities
{
    public class Message : IIdentityObject
    {
        [Key]
        public int Id { get; set; }


        public string Text { get; set; }
        public string LinkPicture { set; get; }

        public User SendByUser { get; set; }

        public Conversation Conversation { get; set; }


        public Message()
        {
        }

        public Message(string Text, string LinkPicture, User SendByUser, Conversation Conversation)
        {
            this.Text = Text;
            this.LinkPicture = LinkPicture;
            this.SendByUser = SendByUser;
            this.Conversation = Conversation;
        }
    }
}
