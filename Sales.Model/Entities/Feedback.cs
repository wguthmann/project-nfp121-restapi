﻿using Sales.Model.Interface;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Sales.Model.Entities
{
    public class Feedback : IIdentityObject
    {
        [Key]
        public int Id { get; set; }

        [AllowNull]
        public string Text { get; set; }

        [Required]
        public float Rate { get; set; }

        [Required]
        public User RaterUser { get; set; }

        [Required]
        public User TargetUser { get; set; }


        public Feedback(string text, float rate, User raterUser, User targetUser)
        {
            Text = text;
            Rate = rate;
            RaterUser = raterUser;
            TargetUser = targetUser;
        }


        public Feedback()
        {
        }
    }
}
