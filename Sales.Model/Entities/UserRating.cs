﻿using Sales.Model.Interface;
using System.ComponentModel.DataAnnotations;

namespace Sales.Model.Entities
{
    public class UserRating : IIdentityObject
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int Rating { get; set; }
    }
}
