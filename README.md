# project-nfp121-api

API : Sale of second-hand games

The idea is quite simple, to allow the connection of person wishing to resell their old games or carry out an exchange.
The application will be realized in the form of Android mobile application.

# Development
## Dotnet migrations

New migration :

```dotnet ef migrations add [migration_name] --project Sales.Model --startup-project SecondLife```

Update :

```dotnet ef database update [migration_name] --project Sales.Model --startup-project SecondLife```

# Contributors

- William GUTHMANN
- Yann HIRSCHMANN
- Kévin KNITTEL
- Louis DEFFINIS

CNAM Strasbourg 2020/2021 - Titre RNCP Concepteur en Architecture Informatique
