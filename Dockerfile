#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.
FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY ./SecondLife/*.csproj ./
# Copy everything else and build
COPY ./ ./

RUN dotnet restore
RUN dotnet publish -c Release -o out


# ENV PATH $PATH:/root/.dotnet/tools
#RUN dotnet tool install -g dotnet-ef --version 3.1.1
# RUN dotnet-ef database update william-migration --project Sales.Model --startup-project SecondLife

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .

ENTRYPOINT ["dotnet", "SecondLife.dll"]