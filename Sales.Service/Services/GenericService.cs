﻿using Microsoft.AspNetCore.JsonPatch;
using Sales.Repositories.Repositories;
using System.Collections.Generic;

namespace Sales.Services.Services
{
    public class GenericService<T> : IService<T> where T : class
    {
        protected IRepository<T> _repo;

        public GenericService(IRepository<T> repo)
        {
            _repo = repo;
        }


        public List<T> List()
        {
            return _repo.All();
        }


        public T Get(in int id)
        {
            return _repo.Get(id);
        }


        public virtual T Add(T sale)
        {
            return _repo.Add(sale);
        }


        public T Patch(in int id, JsonPatchDocument<T> jsonPatch)
        {
            var res = Get(id);
            if (res == null) return null;

            jsonPatch.ApplyTo(res); //result gets the values from the patch request
            _repo.Update(res);

            return res;
        }


        public bool Remove(int id)
        {
            return _repo.Remove(id);
        }
    }
}