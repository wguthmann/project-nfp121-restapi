﻿using Microsoft.IdentityModel.Tokens;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Sales.Services.Services.JwtManager
{
    public class JwtAuthenticationManager : IJwtAuthenticationManager
    {
        private readonly IUserRepository _repo;
        public static readonly string Key = "liqerughaemirujhgklerjbhgfpfiejrhpigaepiuqbhgpiureabipugfbQERPIUBHGUILJAEBPRG";


        public JwtAuthenticationManager(IUserRepository repo)
        {
            _repo = repo;
        }


        public string Authenticate(string usrname, string password)
        {
            User user = _repo.GetBy(username: usrname);

            if (user != null && user.Password == password)
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenKey = Encoding.ASCII.GetBytes(Key);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, usrname)
                    }),
                    Expires = DateTime.UtcNow.AddMinutes(20),
                    SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(tokenKey),
                        SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);

                return tokenHandler.WriteToken(token);
            }

            return null;
        }
    }
}