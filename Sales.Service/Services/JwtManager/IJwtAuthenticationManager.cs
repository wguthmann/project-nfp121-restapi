﻿namespace Sales.Services.Services.JwtManager
{
    public interface IJwtAuthenticationManager
    {
        string Authenticate(string username, string password);
    }
}