﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using System.Collections.Generic;

namespace Sales.Services.Services.ModelsService
{
    public class ProductService : GenericService<Product>
    {
        public ProductService(IRepository<Product> repo) : base(repo)
        {
            _repo = repo;
        }

        public List<Product> GetAllByProductType(ProductType productType)
        {
            return null;
        }
    }
}
