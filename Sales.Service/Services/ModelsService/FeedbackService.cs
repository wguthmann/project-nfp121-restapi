﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.ModelsService
{
    public class FeedbackService : GenericService<Feedback>
    {
        //TODO: get moyenne des feedbacks + retourner liste des commentaires
        public FeedbackService(IRepository<Feedback> repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
