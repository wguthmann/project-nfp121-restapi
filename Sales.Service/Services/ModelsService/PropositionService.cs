﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.ModelsService
{
    public class PropositionService : GenericService<Proposition>
    {
        public PropositionService(IRepository<Proposition> repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
