﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.ModelsService
{
    public class MessageService : GenericService<Message>
    {

        public MessageService(IRepository<Message> repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
