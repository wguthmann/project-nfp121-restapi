﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Repositories.Repositories.ModelsRepository;
using Sales.Services.Validators;
using System.Collections.Generic;

namespace Sales.Services.Services.ModelsService
{
    public class ProductTypeService : GenericService<ProductType>
    {
        private new readonly ProductTypeRepository _repo;

        public ProductTypeService(IRepository<ProductType> repo) : base(repo)
        {
            _repo = repo as ProductTypeRepository;
        }

        public List<ProductType> GetBy(ProductType productType)
        {
            ProductTypeValidator validator = new ProductTypeValidator(_repo);
            if (validator.IsValid(productType))
            {
                var res = _repo.GetBy(productType);
                return res;
            }

            return null;
        }
    }
}
