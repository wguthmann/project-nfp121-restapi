﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Repositories.Repositories.ModelsRepository;
using System.Collections.Generic;

namespace Sales.Services.Services.ModelsService
{
    public class ConversationService : GenericService<Conversation>
    {
        private new ConversationRepository _repo;

        public ConversationService(IRepository<Conversation> repo) : base(repo)
        {
            _repo = repo as ConversationRepository;
        }

        public List<Conversation> GetByUsers(int SendingUserId, int ReceiverUserId)
        {
            return _repo.GetByUsers(SendingUserId, ReceiverUserId);
        }

        public List<Conversation> GetUserImplied(int UserId)
        {
            return _repo.GetUserImplied(UserId);
        }

        public List<Message> GetMessagesByConversation(int ConversationId)
        {
            return _repo.GetMessagesByConversation(ConversationId);
        }
    }
}
