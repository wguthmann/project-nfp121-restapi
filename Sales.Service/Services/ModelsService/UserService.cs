﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Sales.Services.Services.ModelsService
{

    public class UserService : GenericService<User>
    {
        private new IUserRepository _repo;
        private IUserRatingRepository _ratingRepo;

        public UserService(IRepository<User> repo) : base(repo)
        {
            _repo = repo as IUserRepository;
        }


        public List<Feedback> GetFeedback(User user)
        {
            return _repo.GetUserFeedbacks(user);
        }

        public float GetAverageRate(User user)
        {
            return _repo.GetAverageRate(user);
        }

        public bool AddUserRating(int UserId, int Rating)
        {

            return false;
        }

        private bool checkFormat(User user)
        {
            Regex usernameRegex = new Regex("^$");
            Regex emailRegex = new Regex("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

            return false;
        }


        public User ValidateCredentials(string password, string usrname)
        {
            User user = _repo.GetBy(username: usrname);

            if (user != null && user.Password.Equals(password))
                return user;

            return null;
        }

    }
}
