﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;


namespace Sales.Services.Services.ModelsService
{
    public class SaleService : GenericService<Sale>
    {

        public SaleService(IRepository<Sale> repo) : base(repo)
        {
            _repo = repo;
        }


    }

}

