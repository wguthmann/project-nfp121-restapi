﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.ModelsService
{
    public class ProductGenderService : GenericService<ProductGender>
    {

        public ProductGenderService(IRepository<ProductGender> repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
