﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories.ModelsRepository;
using System.Collections.Generic;

namespace Sales.Services.Services.ModelsService
{
    public class PreferencesService
    {
        private UserRepository _userRepo;
        private ProductRepository _productRepository;

        public PreferencesService(UserRepository userRepo, ProductRepository productRepository)
        {
            this._userRepo = userRepo;
            this._productRepository = productRepository;
        }

        public List<ProductType> GetPreferences(int UserId)
        {
            User user = _userRepo.Get(UserId);
            var preferences = _userRepo.getPreferences(user);
            //récupérer la liste des jeux associés aux préférences
            //récupérer la liste des offres relatives aux jeux.
            return null;
        }
    }
}