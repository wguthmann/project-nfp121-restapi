﻿using Microsoft.AspNetCore.JsonPatch;
using System.Collections.Generic;

namespace Sales.Services.Services
{
    public interface IService<T> where T : class
    {
        public List<T> List();
        T Get(in int id);
        T Add(T sale);
        T Patch(in int id, JsonPatchDocument<T> jsonPatch);
        bool Remove(int id);
    }
}
