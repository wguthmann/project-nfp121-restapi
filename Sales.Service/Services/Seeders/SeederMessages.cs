﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederMessages : GenericService<Message>, ISeeder<Message>
    {
        private IRepository<Message> _repoMessage;
        private IRepository<User> _repoUser;
        private IRepository<Conversation> _repoConversation;
        public SeederMessages(IRepository<Message> repoMessage, IRepository<User> repoUser, IRepository<Conversation> repoConversation) : base(repoMessage)
        {
            _repoMessage = repoMessage;
            _repoUser = repoUser;
            _repoConversation = repoConversation;
        }

        public void insertSeed()
        {
            /* Users */
            User user1 = _repoUser.Get(1);
            User user2 = _repoUser.Get(2);
            User user3 = _repoUser.Get(3);
            User user4 = _repoUser.Get(4);
            /* Conversations */
            Conversation conv1 = _repoConversation.Get(1);
            Conversation conv2 = _repoConversation.Get(2);
            Conversation conv3 = _repoConversation.Get(3);
            /* New messages */
            Message m1 = new Message("Puis-je savoir pourquoi vous vendez ce jeu ?", null, user1, conv1);
            Message m2 = new Message("Bonjour est-ce que l'article que vous vendez est toujours disponible ?", null, user3, conv2);
            Message m3 = new Message("Bonjour, effectivement l'article est toujours en vente.", null, user2, conv2);
            Message m4 = new Message("Bonjour, l'état du jeu est-il bon ?", null, user4, conv3);

            _repoMessage.Add(m1);
            _repoMessage.Add(m2);
            _repoMessage.Add(m3);
            _repoMessage.Add(m4);
        }
    }
}
