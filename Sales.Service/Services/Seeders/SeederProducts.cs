﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederProducts : GenericService<Product>, ISeeder<Product>
    {
        public IRepository<Product> _repoProduct;
        public IRepository<User> _repoUser;
        public IRepository<ProductType> _repoProductType;
        public IRepository<City> _repoCity;
        public SeederProducts(IRepository<Product> repoProduct, IRepository<User> repoUser, IRepository<ProductType> repoProductType,
            IRepository<City> repoCity) : base(repoProduct)
        {
            _repoProduct = repoProduct;
            _repoUser = repoUser;
            _repoProductType = repoProductType;
            _repoCity = repoCity;
        }

        public void insertSeed()
        {
            /* Users */
            User user1 = _repoUser.Get(1);
            User user2 = _repoUser.Get(2);
            User user3 = _repoUser.Get(3);
            User user4 = _repoUser.Get(4);
            /* IdCity */
            City city1 = _repoCity.Get(1);
            City city2 = _repoCity.Get(2);
            City city3 = _repoCity.Get(3);
            City city4 = _repoCity.Get(4);
            City city5 = _repoCity.Get(5);
            City city6 = _repoCity.Get(6);
            /* ProductType */
            ProductType pdType1 = _repoProductType.Get(1);
            ProductType pdType2 = _repoProductType.Get(2);
            ProductType pdType3 = _repoProductType.Get(3);
            ProductType pdType4 = _repoProductType.Get(4);
            ProductType pdType5 = _repoProductType.Get(5);
            ProductType pdType6 = _repoProductType.Get(6);
            ProductType pdType7 = _repoProductType.Get(7);
            /* New products */
            // Matches with feedback seeders
            Product pd1 = new Product("GTAIV", "Vends jeu GTAIV en très bon état avec sa pochette d'origine", 5.9f, city2, user1, pdType1, true);
            Product pd2 = new Product("Halo4", "Code Xbox live pour Halo4", 10.0f, city5, user2, pdType4, false);
            Product pd3 = new Product("Farming Simulator", "Jeu PC Farming Simulator avec tous les éléments d'origine", 15.0f, city3, user4, pdType5, true);
            Product pd4 = new Product("Cyberpunk 2077", "Code de jeu PC pour Cyberpunk 2077 plateforme GOG", 57.0f, city4, user3, pdType6, false);
            // New sales
            Product pd5 = new Product("Assassin's creed Odyssey", "Vends jeu Assassin's Creeed Odyssey en très bon état avec sa pochette d'origine", 23.0f, city2, user1, pdType1, true);
            Product pd6 = new Product("NeedForSpeed Payout", "NeedForSpeed Payout boîte un peu cassé mais CD niquel", 9.0f, city3, user4, pdType5, true);
            Product pd7 = new Product("SuperMarioBros", "Jeu super et convivial pour des parties entre amis", 12.0f, city5, user2, pdType7, true);

            _repoProduct.Add(pd1);
            _repoProduct.Add(pd2);
            _repoProduct.Add(pd3);
            _repoProduct.Add(pd4);
            _repoProduct.Add(pd5);
            _repoProduct.Add(pd6);
            _repoProduct.Add(pd7);
        }
    }
}
