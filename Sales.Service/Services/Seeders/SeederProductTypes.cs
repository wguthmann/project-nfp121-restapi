﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederProductTypes : GenericService<ProductType>, ISeeder<ProductType>
    {
        private IRepository<ProductType> _repoProductType;
        public SeederProductTypes(IRepository<ProductType> repoProductType) : base(repoProductType)
        {
            _repoProductType = repoProductType;
        }

        public void insertSeed()
        {
            ProductType pdType1 = new ProductType("Jeu PS3", 1, null);
            ProductType pdType2 = new ProductType("Code jeu PS3", 2, null);
            ProductType pdType3 = new ProductType("Jeu Xbox", 3, null);
            ProductType pdType4 = new ProductType("Code jeu Xbox", 4, null);
            ProductType pdType5 = new ProductType("Jeu PC", 5, null);
            ProductType pdType6 = new ProductType("Code jeu PC", 6, null);
            ProductType pdType7 = new ProductType("Autre", 7, null);

            _repoProductType.Add(pdType1);
            _repoProductType.Add(pdType2);
            _repoProductType.Add(pdType3);
            _repoProductType.Add(pdType4);
            _repoProductType.Add(pdType5);
            _repoProductType.Add(pdType6);
            _repoProductType.Add(pdType7);
        }
    }
}
