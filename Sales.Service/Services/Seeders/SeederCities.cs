﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederCities : GenericService<City>, ISeeder<City>
    {
        private new IRepository<City> _repo;

        public SeederCities(IRepository<City> repo) : base(repo)
        {
            _repo = repo;
        }

        public void insertSeed()
        {
            City city1 = new City("Strasbourg", "France");
            City city2 = new City("Mulhouse", "France");
            City city3 = new City("Colmar", "France");
            City city4 = new City("Haguenau", "France");
            City city5 = new City("Saverne", "France");
            City city6 = new City("Molsheim", "France");
            _repo.Add(city1);
            _repo.Add(city2);
            _repo.Add(city3);
            _repo.Add(city4);
            _repo.Add(city5);
            _repo.Add(city6);
        }
    }
}
