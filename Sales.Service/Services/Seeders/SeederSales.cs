﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederSales : GenericService<Sale>, ISeeder<Sale>
    {
        public IRepository<Sale> _repoSale;
        public IRepository<User> _repoUser;
        public IRepository<Product> _repoProduct;

        public SeederSales(IRepository<Sale> repoSale, IRepository<User> repoUser, IRepository<Product> repoProduct) : base(repoSale)
        {
            _repoSale = repoSale;
            _repoUser = repoUser;
            _repoProduct = repoProduct;
        }

        public void insertSeed()
        {
            /* Users */
            User user1 = _repoUser.Get(1);
            User user2 = _repoUser.Get(2);
            User user3 = _repoUser.Get(3);
            User user4 = _repoUser.Get(4);
            /* Products */
            Product pd1 = _repoProduct.Get(1);
            Product pd2 = _repoProduct.Get(2);
            Product pd3 = _repoProduct.Get(3);
            Product pd4 = _repoProduct.Get(4);
            /* New sales */
            Sale s1 = new Sale(5.0f, user1, user2, pd1);
            Sale s2 = new Sale(8.0f, user1, user4, pd2);
            Sale s3 = new Sale(15.0f, user2, user3, pd3);
            Sale s4 = new Sale(57.0f, user3, user1, pd4);
            _repoSale.Add(s1);
            _repoSale.Add(s2);
            _repoSale.Add(s3);
            _repoSale.Add(s4);
        }
    }
}
