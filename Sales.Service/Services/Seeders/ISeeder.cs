﻿namespace Sales.Services.Services.Seeders
{
    public interface ISeeder<T> where T : class
    {
        public void insertSeed();

    }
}
