﻿using Sales.Model.Entities;

namespace Sales.Services.Services.Seeders
{
    public class GlobalSeeder
    {
        private ISeeder<City> _seederCities;
        private ISeeder<User> _seederUsers;
        private ISeeder<Conversation> _seederConversations;
        private ISeeder<Message> _seederMessages;
        private ISeeder<ProductGender> _seederProductGenders;
        private ISeeder<Product> _seederProducts;

        private ISeeder<Sale> _seederSales;
        private ISeeder<ProductType> _seederProductTypes;
        private ISeeder<Proposition> _seederProposition;


        public GlobalSeeder(ISeeder<City> seederCities, ISeeder<User> seederUsers, ISeeder<Product> seederProducts,
                            ISeeder<Sale> seederSales, ISeeder<ProductType> seederProductTypes, ISeeder<Proposition> seederProposition,
                            ISeeder<Conversation> seederConversations, ISeeder<Message> seederMessages)
        {
            _seederCities = seederCities;
            _seederUsers = seederUsers;
            //_seederProductGenders = seederProductGenders;
            _seederConversations = seederConversations;
            _seederMessages = seederMessages;
            _seederProductTypes = seederProductTypes;
            _seederProducts = seederProducts;
            _seederSales = seederSales;
            _seederProposition = seederProposition;

        }

        public void run()
        {
            _seederCities.insertSeed();
            _seederUsers.insertSeed();
            //_seederProductGenders.insertSeed();
            _seederConversations.insertSeed();
            _seederMessages.insertSeed();
            _seederProductTypes.insertSeed();
            _seederProducts.insertSeed();
            _seederSales.insertSeed();
            _seederProposition.insertSeed();

        }
    }
}
