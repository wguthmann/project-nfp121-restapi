﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederConversations : GenericService<Conversation>, ISeeder<Conversation>
    {
        public IRepository<Conversation> _repoConversation;
        public IRepository<User> _repoUser;
        public SeederConversations(IRepository<Conversation> repoConversation, IRepository<User> repoUser) : base(repoConversation)
        {
            _repoConversation = repoConversation;
            _repoUser = repoUser;
        }

        public void insertSeed()
        {
            /* Users */
            User user1 = _repoUser.Get(1);
            User user2 = _repoUser.Get(2);
            User user3 = _repoUser.Get(3);
            User user4 = _repoUser.Get(4);

            /* Conversations */
            Conversation conversation1 = new Conversation(user1, user2);
            Conversation conversation2 = new Conversation(user2, user3);
            Conversation conversation3 = new Conversation(user3, user4);
            Conversation conversation4 = new Conversation(user4, user1);

            _repoConversation.Add(conversation1);
            _repoConversation.Add(conversation2);
            _repoConversation.Add(conversation3);
            _repoConversation.Add(conversation4);

        }
    }
}
