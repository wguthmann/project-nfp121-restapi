﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederUsers : GenericService<User>, ISeeder<User>
    {
        private IRepository<User> _repoUser;
        private IRepository<City> _repoCity;

        public SeederUsers(IRepository<User> repoUser, IRepository<City> repoCity) : base(repoUser)
        {
            _repoUser = repoUser;
            _repoCity = repoCity;
        }

        public void insertSeed()
        {
            City city1 = _repoCity.Get(1);
            City city2 = _repoCity.Get(2);
            City city3 = _repoCity.Get(3);
            City city4 = _repoCity.Get(4);
            User user1 = new User("kk", "kknittel@gmail.com", "moulaga", city1);
            User user2 = new User("yh", "yhirschmann@gmail.com", "goulaga", city2);
            User user3 = new User("ld", "ldeffinis@gmail.com", "poulaga", city3);
            User user4 = new User("wg", "wguthmann@gmail.com", "roulaga", city4);
            _repoUser.Add(user1);
            _repoUser.Add(user2);
            _repoUser.Add(user3);
            _repoUser.Add(user4);
        }
    }
}
