﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederProductGenders : GenericService<ProductGender>, ISeeder<ProductGender>
    {
        private IRepository<ProductGender> _repoProductGender;
        public SeederProductGenders(IRepository<ProductGender> repoProductGender) : base(repoProductGender)
        {
            _repoProductGender = repoProductGender;
        }

        public void insertSeed()
        {
            ProductGender pdGender1 = new ProductGender("RPG");
            ProductGender pdGender2 = new ProductGender("FPS");
            ProductGender pdGender3 = new ProductGender("TPS");
            ProductGender pdGender4 = new ProductGender("FreeToPlay");

            _repoProductGender.Add(pdGender1);
            _repoProductGender.Add(pdGender2);
            _repoProductGender.Add(pdGender3);
            _repoProductGender.Add(pdGender4);

        }
    }
}
