﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederFeedbacks : GenericService<Feedback>, ISeeder<Feedback>
    {
        private IRepository<Feedback> _repoFeedback;
        private IRepository<User> _repoUser;

        public SeederFeedbacks(IRepository<Feedback> repoFeedback, IRepository<User> repoUser) : base(repoFeedback)
        {
            _repoFeedback = repoFeedback;
            _repoUser = repoUser;
        }

        public void insertSeed()
        {
            User user1 = _repoUser.Get(1);
            User user2 = _repoUser.Get(2);
            User user3 = _repoUser.Get(3);
            User user4 = _repoUser.Get(4);
            Feedback f1 = new Feedback("Sérieux.", 4.5f, user1, user2);
            Feedback f2 = new Feedback("Conforme à mes attentes", 4.0f, user1, user4);
            Feedback f3 = new Feedback("N'a pas voulu baissé son prix mais sinon correct", 3.8f, user2, user3);
            Feedback f4 = new Feedback("Vendeur mal honnête, m'a vendu un produit très usagé pour presque neuf", 1.0f, user3, user1);
            _repoFeedback.Add(f1);
            _repoFeedback.Add(f2);
            _repoFeedback.Add(f3);
            _repoFeedback.Add(f4);
        }
    }
}
