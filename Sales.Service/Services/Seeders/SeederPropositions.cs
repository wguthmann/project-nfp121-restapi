﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.Seeders
{
    public class SeederPropositions : GenericService<Proposition>, ISeeder<Proposition>
    {
        public IRepository<Proposition> _repoProposition;
        public IRepository<User> _repoUser;
        public IRepository<Sale> _repoSale;
        public IRepository<Product> _repoProduct;

        public SeederPropositions(IRepository<Proposition> repoProposition, IRepository<User> repoUser, IRepository<Sale> repoSale, IRepository<Product> repoProduct) : base(repoProposition)
        {
            _repoProposition = repoProposition;
            _repoUser = repoUser;
            _repoSale = repoSale;
            _repoProduct = repoProduct;

        }

        public void insertSeed()
        {
            /* Users */
            User user1 = _repoUser.Get(1);
            User user2 = _repoUser.Get(2);
            User user3 = _repoUser.Get(3);
            User user4 = _repoUser.Get(4);
            /* Sales */
            Sale sale1 = _repoSale.Get(1);
            Sale sale2 = _repoSale.Get(2);
            Sale sale3 = _repoSale.Get(3);
            Sale sale4 = _repoSale.Get(4);
            /* Products */
            Product product1 = _repoProduct.Get(1);
            Product product2 = _repoProduct.Get(2);
            Product product3 = _repoProduct.Get(3);
            Product product4 = _repoProduct.Get(4);
            /* Props */
            Proposition prop1 = new Proposition(15.0f, product1, false, user1, sale1);
            Proposition prop2 = new Proposition(22.0f, product2, null, user2, sale2);
            Proposition prop3 = new Proposition(36.0f, product3, true, user4, sale3);
            Proposition prop4 = new Proposition(7.0f, product4, null, user3, sale4);

            _repoProposition.Add(prop1);
            _repoProposition.Add(prop2);
            _repoProposition.Add(prop3);
            _repoProposition.Add(prop4);
        }
    }
}
