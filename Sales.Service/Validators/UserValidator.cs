﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators.Generics;
using System;

namespace Sales.Services.Validators
{
    public class UserValidator : IValidator<User>
    {
        private IRepository<User> _repo;

        public UserValidator(IRepository<User> repo)
        {
            _repo = repo;
        }

        public bool CanAdd(User obj)
        {
            return _repo.FindOne(user =>
               user.EmailAddress.Equals(obj.EmailAddress, StringComparison.InvariantCultureIgnoreCase)
               ) == null && _repo.FindOne(user =>
                user.Username.Equals(obj.Username)
               ) == null;
        }

        public bool CanEdit(User obj)
        {
            return true;
        }
    }
}
