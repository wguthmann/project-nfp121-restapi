﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators.Generics;

namespace Sales.Services.Validators
{
    public class PropositionValidator : IValidator<Proposition>
    {
        private IRepository<Proposition> _repo;
        private IRepository<User> _repoUser;

        public PropositionValidator(IRepository<Proposition> repo, IRepository<User> repoUser)
        {
            _repo = repo;
            _repoUser = repoUser;
        }

        public bool CanAdd(Proposition proposition)
        {
            return _repoUser.FindOne(user =>
            user == proposition.ProposerUser
            ) != null;

        }

        public bool CanEdit(Proposition obj)
        {
            return true;
        }
    }
}
