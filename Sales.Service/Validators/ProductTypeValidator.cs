﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators.Generics;
using System;

namespace Sales.Services.Validators
{
    public class ProductTypeValidator : IValidator<ProductType>
    {

        private IRepository<ProductType> _repo;

        public ProductTypeValidator(IRepository<ProductType> repo)
        {
            _repo = repo;
        }

        public bool CanAdd(ProductType obj)
        {
            return _repo.FindOne(productType =>
                productType.Title.Equals(obj.Title, StringComparison.InvariantCultureIgnoreCase)
            ) == null;
        }

        public bool CanEdit(ProductType obj)
        {
            return false;
        }

        public bool IsValid(ProductType obj)
        {
            return obj.Title != "";
        }
    }
}
