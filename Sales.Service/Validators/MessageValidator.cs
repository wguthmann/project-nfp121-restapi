﻿using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators.Generics;

namespace Sales.Services.Validators
{
    public class MessageValidator : IValidator<Message>
    {
        private IRepository<Message> _repo;
        private IRepository<Conversation> _repoConv;

        public MessageValidator(IRepository<Message> repo, IRepository<Conversation> repoConv)
        {
            _repo = repo;
            _repoConv = repoConv;
        }

        public bool CanAdd(Message msg)
        {
            //la conv du message existe et l'user qui l'envoie appartient à cette conv (receiver ou sender)  
            return _repoConv.FindOne(conv =>
            (conv.Equals(msg.Conversation)) && (msg.SendByUser.Equals(conv.ReceiverUser) || msg.SendByUser.Equals(conv.SendingUser))
            ) != null;

        }
        public bool CanEdit(Message msg)
        {
            return true;
        }
    }
}
