﻿namespace Sales.Services.Validators.Generics
{
    public interface IValidator<T> where T : class
    {
        bool CanAdd(T obj);
        bool CanEdit(T obj);
    }
}