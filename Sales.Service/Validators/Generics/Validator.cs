﻿namespace Sales.Services.Validators.Generics
{
    public class Validator<T> : IValidator<T> where T : class
    {
        public bool CanAdd(T obj)
        {
            return true;
        }

        public bool CanEdit(T obj)
        {
            return true;
        }
    }
}

