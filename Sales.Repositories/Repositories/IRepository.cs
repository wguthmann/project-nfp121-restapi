﻿using Sales.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Sales.Repositories.Repositories
{
    public interface IConversationRepository : IRepository<Conversation>
    {
        List<Conversation> GetByUsers(int SendinguserId, int ReceiverUserId);
        List<Conversation> GetUserImplied(int UserId);
        List<Message> GetMessagesByConversation(int ConversationId);
    }

    public interface IMessageRepository : IRepository<Message>
    {
    }

    public interface IProductGenderRepository : IRepository<ProductGender>
    {
    }

    public interface IUserRepository : IRepository<User>
    {
        List<Feedback> GetUserFeedbacks(User user);

        User GetBy(int id = 0, string username = null, string email = null);

        List<ProductGender> getPreferences(User user);

        float GetAverageRate(User user);
    }

    public interface IUserRatingRepository : IRepository<UserRating>
    {

    }

    public interface IPropositionRepository : IRepository<Proposition>
    {
    }

    public interface IProductTypeRepository : IRepository<ProductType>
    {
        List<ProductType> GetBy(ProductType productType);
    }

    public interface IProductRepository : IRepository<Product>
    {
    }

    public interface IFeedbackRepository : IRepository<Feedback>
    {
    }

    public interface ICityRepository : IRepository<City>
    {
    }

    public interface IRepository<T> where T : class
    {
        List<T> All();
        T Get(int id);
        T Add(T sale);
        T Update(T sale);
        bool Remove(int id);
        bool Exists(T sale);
        T FindOne(Expression<Func<T, bool>> conditon);
    }
}
