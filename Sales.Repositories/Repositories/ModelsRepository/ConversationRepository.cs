﻿using Sales.Model;
using Sales.Model.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class ConversationRepository : GenericRepository<Conversation>, IConversationRepository
    {
        private readonly SaleDbContext _context;

        public ConversationRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }

        public List<Conversation> GetByUsers(int SendingUserId, int ReceiverUserId)
        {
            List<Conversation> conv = _context.Conversation.Where(c => c.SendingUser.Id == SendingUserId && c.ReceiverUser.Id == ReceiverUserId).ToList();
            return conv;
        }

        public List<Conversation> GetUserImplied(int UserId)
        {
            List<Conversation> conv = _context.Conversation.Where(c => c.SendingUser.Id == UserId || c.ReceiverUser.Id == UserId).ToList();
            return conv;
        }

        public List<Message> GetMessagesByConversation(int ConversationId)
        {
            List<Message> messages = _context.Message.Where(m => m.Conversation.Id == ConversationId).ToList();
            return messages;
        }

    }
}
