﻿using Sales.Model;
using Sales.Model.Entities;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class PropositionRepository : GenericRepository<Proposition>, IPropositionRepository
    {
        private readonly SaleDbContext _context;

        public PropositionRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
