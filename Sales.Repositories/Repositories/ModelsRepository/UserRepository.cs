﻿using Sales.Model;
using Sales.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly SaleDbContext _context;

        public UserRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }


        public List<Feedback> GetUserFeedbacks(User user)
        {
            List<Feedback> userFeedbacks = _context.Feedback.Where(r => r.TargetUser == user).ToList();
            return userFeedbacks;
        }


        public User GetBy(int id = 0, string username = null, string email = null)
        {
            if (id != 0) // Get by ID
                return _context.User.Where(u => u.Id == id).FirstOrDefault();

            if (username != null) // Get by username
                return _context.User.Where(u => u.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();


            if (email != null) // Get by email
                return _context.User.Where(u => u.EmailAddress.Equals(email, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

            return null;
        }


        public List<ProductGender> getPreferences(User user)
        {
            return _context.User.FirstOrDefault(u => u.Id == user.Id).Preferences;
        }

        public float GetAverageRate(User user)
        {
            float averageRate = _context.Feedback.Where(r => r.TargetUser == user).Average(r => r.Rate);
            return averageRate;
        }
    }
}
