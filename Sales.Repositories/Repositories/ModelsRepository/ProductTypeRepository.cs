﻿using Sales.Model;
using Sales.Model.Entities;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class ProductTypeRepository : GenericRepository<ProductType>, IProductTypeRepository
    {

        private readonly SaleDbContext _context;

        public ProductTypeRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }

        public List<ProductType> GetBy(ProductType product)
        {
            var result = _context.ProductType.Where(p =>
                (product.Rating == 0 || p.Rating == product.Rating) &&
                (product.Title == null || p.Title == product.Title));
            return result as List<ProductType>;
        }

        public List<ProductType> GetByGender(ProductGender productGender)
        {
            var result = _context.ProductType.Where(p => p.Genders.Contains(productGender)).ToList();
            return result;
        }
    }

}
