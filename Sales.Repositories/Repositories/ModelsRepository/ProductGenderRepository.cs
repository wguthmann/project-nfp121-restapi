﻿using Sales.Model;
using Sales.Model.Entities;
using System;
using System.Linq;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class ProductGenderRepository : GenericRepository<ProductGender>
    {
        private readonly SaleDbContext _context;

        public ProductGenderRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }

        public ProductGender GetByType(String type)
        {
            return _context.ProductGender.FirstOrDefault(p => (p.Type == type));
        }
    }
}
