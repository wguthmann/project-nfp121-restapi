﻿using Sales.Model;
using Sales.Model.Entities;

namespace Sales.Repositories.Repositories
{
    public class UserRatingRepository : GenericRepository<UserRating>, IUserRatingRepository
    {
        public UserRatingRepository(SaleDbContext context) : base(context)
        {
        }
    }
}
