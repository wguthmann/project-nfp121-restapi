﻿using Sales.Model;
using Sales.Model.Entities;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class MessageRepository : GenericRepository<Message>, IMessageRepository
    {
        private readonly SaleDbContext _context;

        public MessageRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
