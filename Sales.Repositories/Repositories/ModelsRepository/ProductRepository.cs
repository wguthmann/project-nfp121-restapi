﻿using Sales.Model;
using Sales.Model.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly SaleDbContext _context;

        public ProductRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }

        public List<Product> GetByPreferences(List<ProductGender> productGenders)
        {
            var genderIds = productGenders.Select(p => p.Id).ToList();
            var queryResult = _context.Product.Where(p =>
                                                        genderIds.Any(
                                                            pg => p.ProductType.Genders.Any(ppg => ppg.Id == pg))
                                                        ).ToList();
            return queryResult.ToList();
        }
    }
}
