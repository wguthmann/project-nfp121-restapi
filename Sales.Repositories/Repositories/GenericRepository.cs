﻿using Sales.Model;
using Sales.Model.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Sales.Repositories.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : class, IIdentityObject
    {
        private readonly SaleDbContext _context;

        public GenericRepository(SaleDbContext context)
        {
            _context = context;
        }

        public List<T> All()
        {
            return _context.Set<T>().ToList();
        }

        public T Get(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public T Add(T obj)
        {
            _context.Add(obj);
            _context.SaveChanges();
            return obj;
        }

        public T Update(T obj)
        {
            _context.Attach(obj);
            _context.SaveChanges();
            return obj;
        }

        public bool Remove(int id)
        {
            var obj = Get(id);
            if (obj == null) return false;

            _context.Remove(obj);
            _context.SaveChanges();
            return Get(id) == null;
        }

        public bool Exists(T obj)
        {
            var res = _context.Set<T>().FirstOrDefault(x => x.Id == obj.Id);
            return res != null;
        }

        public T FindOne(Expression<Func<T, bool>> conditon)
        {
            return _context.Set<T>().FirstOrDefault(conditon);
        }
    }
}
