﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sales.Model.Entities;
using Sales.Services.Services;

namespace Sales.Services.Tests.Services
{
    [TestClass]
    public class UserServiceTester
    {
        private Mock<IService<User>> _serviceMock;

        public UserServiceTester()
        {
            _serviceMock = new Mock<IService<User>>();
        }

        [TestMethod]
        public void CanCreate_WithUser_ThenTrue()
        {
            User user = new User("Test", "test@gmail.com", "password", new City());
            Assert.IsNotNull(_serviceMock.Setup(u => u.Add(new User("Test", "test@gmail.com", "password", new City()))));
        }


    }
}
