﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators;
using System;
using System.Linq.Expressions;

namespace Sales.Services.Tests.Validators
{
    [TestClass]
    class ProductTypeValidatorTester
    {

        private ProductTypeValidator _validator;
        private Mock<IRepository<ProductType>> _repoMock;

        public ProductTypeValidatorTester()
        {
            _repoMock = new Mock<IRepository<ProductType>>();
            _validator = new ProductTypeValidator(_repoMock.Object);
        }

        [TestMethod]
        public void CanAdd_WithNullProductType_ThenTrue()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<ProductType, bool>>>()
            )).Returns((ProductType)null);


            Assert.IsTrue(_validator.CanAdd(new ProductType()));
        }

        [TestMethod]
        public void CanAdd_WithExistingProductType_ThenFalse()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<ProductType, bool>>>()
            )).Returns(new ProductType());

            Assert.IsFalse(_validator.CanAdd(new ProductType()));
        }

        [TestMethod]
        public void CanAdd_WithAlreadyExistingTitle_ThenFalse()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<ProductType, bool>>>()
            )).Returns(new ProductType("titre", 1, null));

            Assert.IsFalse(_validator.CanAdd(new ProductType("titre", 1, null)));
        }

        [TestMethod]
        public void CanAdd_WithNonAlreadyExistingTitle_ThenTrue()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<ProductType, bool>>>()
            )).Returns(new ProductType("titre", 1, null));

            Assert.IsTrue(_validator.CanAdd(new ProductType("titre différent", 1, null)));
        }

        [TestMethod]
        public void IsValid_WithEmptyTitle_thenFalse()
        {
            Assert.IsFalse(_validator.IsValid(new ProductType("", 2, null)));
        }

        [TestMethod]
        public void IsValid_WithTitreAsTitle_thenTrue()
        {
            Assert.IsTrue(_validator.IsValid(new ProductType("titre", 2, null)));
        }

    }
}

