using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators;
using System;
using System.Linq.Expressions;

namespace Sales.Services.Tests.Validators
{
    [TestClass]
    public class CityValidatorTester
    {
        private CityValidator _validator;
        private Mock<IRepository<City>> _repoMock;

        public CityValidatorTester()
        {
            _repoMock = new Mock<IRepository<City>>();
            _validator = new CityValidator(_repoMock.Object);
        }

        [TestMethod]
        public void CanAdd_WithAnyCitiesInDb_ThenTrue()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<City, bool>>>()
            )).Returns((City)null);


            Assert.IsTrue(_validator.CanAdd(new City()));
        }

        [TestMethod]
        public void CanAdd_WithExistingCityInDb_ThenFalse()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<City, bool>>>()
            )).Returns(new City());

            Assert.IsFalse(_validator.CanAdd(new City()));
        }

    }
}
