﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators;
using System;
using System.Linq.Expressions;

namespace Sales.Services.Tests.Validators
{
    [TestClass]
    public class PropositionValidatorTester
    {

        private PropositionValidator _validator;
        private Mock<IRepository<Proposition>> _repoMock;
        private Mock<IRepository<User>> _repoUserMock;

        public PropositionValidatorTester()
        {
            _repoMock = new Mock<IRepository<Proposition>>();
            _repoUserMock = new Mock<IRepository<User>>();
            _validator = new PropositionValidator(_repoMock.Object, _repoUserMock.Object);
        }

        [TestMethod]
        public void CanAdd_WithExistingUserProposer_ThenTrue()
        {

            User testUser = new User("billy", "a@a", "pass", null);

            _repoUserMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<User, bool>>>()
            )).Returns(testUser);

            Assert.IsTrue(_validator.CanAdd(new Proposition(1, null, null, testUser, new Sale())));
        }

        [TestMethod]
        public void CanAdd_WithoutExistingUserProposer_Thenfalse()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<Proposition, bool>>>()
            )).Returns((Proposition)null);

            User testUser = new User("billy", "a@a", "pass", null);
            Assert.IsFalse(_validator.CanAdd(new Proposition(1, null, null, testUser, new Sale())));
        }
    }
}
