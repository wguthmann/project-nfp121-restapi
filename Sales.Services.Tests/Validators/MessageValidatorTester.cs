﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators;
using System;
using System.Linq.Expressions;

namespace Sales.Services.Tests.Validators
{
    [TestClass]
    public class MessageValidatorTester
    {
        private MessageValidator _validator;
        private Mock<IRepository<Message>> _repoMock;
        private Mock<IRepository<Conversation>> _repoConvMock;

        public MessageValidatorTester()
        {
            _repoMock = new Mock<IRepository<Message>>();
            _repoConvMock = new Mock<IRepository<Conversation>>();
            _validator = new MessageValidator(_repoMock.Object, _repoConvMock.Object);
        }

        [TestMethod]
        public void CanAdd_WithNonExistingConversation_ThenFalse()
        {
            User user1 = new User();
            Conversation conv1 = new Conversation();

            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<Message, bool>>>()
            )).Returns((Message)null);

            Assert.IsFalse(_validator.CanAdd(new Message("salut", "", user1, conv1)));
        }

        [TestMethod]
        public void CanAdd_WithoutUserInConversation_ThenFalse()
        {
            User user1 = new User("billy", "billy@mail", "pass", new City());
            User user2 = new User("john", "john@mail", "pass", new City());
            User user3 = new User("stevens", "stevens@mail", "pass", new City());
            Conversation conv1 = new Conversation(user1, user2);

            _repoConvMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<Conversation, bool>>>()
            )).Returns(conv1);

            Assert.IsFalse(_validator.CanAdd(new Message("salut", "", user3, conv1)));
        }

        [TestMethod]
        public void CanAdd_WithUserInConversation_ThenTrue()
        {
            User user1 = new User("billy", "billy@mail", "pass", new City());
            User user2 = new User("john", "john@mail", "pass", new City());
            Conversation conv1 = new Conversation(user1, user2);

            _repoConvMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<Conversation, bool>>>()
            )).Returns(new Conversation(user1, user2));

            Assert.IsTrue(_validator.CanAdd(new Message("salut", "", user1, conv1)));
        }

    }
}
