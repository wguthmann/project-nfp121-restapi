﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators;
using System;
using System.Linq.Expressions;

namespace Sales.Services.Tests.Validators
{
    [TestClass]
    public class UserValidatorTester
    {
        private UserValidator _validator;
        private Mock<IRepository<User>> _repoMock;


        public UserValidatorTester()
        {
            _repoMock = new Mock<IRepository<User>>();
            _validator = new UserValidator(_repoMock.Object);
        }

        [TestMethod]
        public void CanAdd_WithAnyUser_ThenTrue()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<User, bool>>>()
            )).Returns((User)null);


            Assert.IsTrue(_validator.CanAdd(new User()));
        }

        [TestMethod]
        public void CanAdd_WithExistingUser_ThenFalse()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<User, bool>>>()
            )).Returns(new User());

            Assert.IsFalse(_validator.CanAdd(new User()));
        }

        [TestMethod]
        public void CanAdd_WithExistingUsername_ThenFalse()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<User, bool>>>()
            )).Returns(new User("Toto", "tot@gmail.com", "password", new City()));

            Assert.IsFalse(_validator.CanAdd(new User("Toto", "jean@gmail.com", "password", new City())));
        }


        [TestMethod]
        public void CanAdd_With2DifferentUserSameEmail_ThenFalse()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<User, bool>>>()
            )).Returns(new User("Jean", "test@gmail.com", "password", new City()));

            Assert.IsFalse(_validator.CanAdd(new User("Fred", "test@gmail.com", "123456", new City())));
        }

        [TestMethod]
        public void CanAdd_With2DifferentUserSameUsername_ThenFalse()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<User, bool>>>()
            )).Returns(new User("Jean-Kévin", "adressechaude@hotmail.com", "password", new City()));

            Assert.IsFalse(_validator.CanAdd(new User("Jean-Kévin", "xXx_JKdu54_xXx@hotmail.com", "123456", new City())));
        }

        [TestMethod]
        public void CanEdit_WithUser_ThenTrue()
        {
            Assert.IsTrue(_validator.CanEdit(new User()));
        }
    }
}
